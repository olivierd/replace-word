#!/bin/sh

# Replace word by another.
#

# Keyword to search
KEYWORD="documentation_draft"


if [ $# -eq 1 ]; then
	# File or directory?
	if [ -d $1 ]; then
		for file in $(find $1 -type f); do
			grep ${KEYWORD} ${file} && \
				sed -i'' -e "s|${KEYWORD}|${KEYWORD%_*}|g" ${file}
		done
	elif [ -e $1 ]; then
		grep ${KEYWORD} $1 && \
			sed -i'' -e "s|${KEYWORD}|${KEYWORD%_*}|g" $1
	else
		echo "No such file or directory"
	fi
else
	echo "Usage: ./$0 FILE or DIRECTORY"
fi

